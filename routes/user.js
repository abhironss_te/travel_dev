const express = require('express');
const router = express.Router();

var mysql = require('mysql');
//MySQL details
var mysqlConnection = mysql.createConnection({
    port: 3306,
    connectionLimit: 10,
    acquireTimeout: 30000, //30 secs
    host: '159.65.170.0',
    user: 'abhishek',
    password: 'abhishek@123',
    database: 'travel_data',
    multipleStatements: true
});
mysqlConnection.connect((err)=> {   
if(!err)
    console.log('Connection Established Successfully');
else
    console.log('Connection Failed!'+ JSON.stringify(err,undefined,2));
}); 

const Joi = require('joi');
router.post('/add', (req, res) => {
    const { error } = validateuser(req.body)
    if(error){
        res.status(422).send(error)
    }else{
        console.log(req.body)
    }
})


function validateuser(user){
    const schema = {
        name : Joi.string().min(3).required().regex(/^[a-zA-Z ]+$/),
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        phone: Joi.string().alphanum().min(10).required().regex(/^[0-9]*$/),
        password: Joi.string().required(),
        gender: Joi.string().required(),
        dob: Joi.string().required(),
        country: Joi.string().required().regex(/^[0-9]*$/),
        state: Joi.string().required().regex(/^[0-9]*$/),
        about: Joi.string().required(),
        alchohal: Joi.string().required(),
        smoke: Joi.string().required(),
        relationship: Joi.string().required(),
        children: Joi.string().required(),
        profile_image: Joi.string().required()
    }
    return Joi.validate(user, schema);
}
module.exports = router;









